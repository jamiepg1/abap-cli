abap-cli
========

A simple script to deploy code to a application server, run it, and return results. 
Don't use it on production servers.

`npm install request` after clone. 

![](screenshots/abapcode.png)

![](screenshots/run.PNG)
